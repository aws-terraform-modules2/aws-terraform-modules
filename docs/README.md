
# Design

# Amazon ECS
*[Amazon ECS official Docs](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/Welcome.html)*

Amazon ECS  is a fully managed container orchestration service that helps you easily deploy, manage, and scale containerized applications. Amazon ECS comes with AWS configuration and operational best practices built-in. This makes it easier for teams to focus on building the applications, not the environment.

*The main purpose of an orchestrator is to manage the lifecycle of containers (Create/restart/destroy)*

## Application lifecycle on ECS
You must architect your applications so that they can run on containers.
- A container holds everything that your software application requires to run. This includes relevant code, runtime, system tools, and system libraries.
- Containers are created from a read-only template that's called an image, typically built from a Dockerfile. After they're built, these images are stored in a registry such as Amazon ECR where they can be downloaded from.
- After you create and store your image, you create an Amazon ECS task definition. It is a text file in JSON format that describes the parameters and one or more containers that form your application. For example, you can use it to specify the image and parameters for the operating system, which ports to open for your application, and what data volumes to use with the containers in the task.
- After you define your task definition, you deploy it as either a service or a task on your cluster.
- After you deploy the task or service, you can use any of the following tools to monitor your deployment and application: CloudWatch, Runtime Monitoring

## ECS main components

> #### ECS capacity
>
> Amazon ECS capacity is the infrastructure where your containers run.
>
> Capacity options: EC2 instances, Serverless AWS Fargate, On-premises virtual machines.
>
> The capacity can be located in any of the following AWS resources: Availability Zones, Local Zones, Wavelength Zones, AWS Regions, AWS Outposts.

> #### ECS Cluster are Logical Grouping of EC2 Instances or Fargate (Serverless, pay-as-you-go compute engine.)
>
> #### ECS Agent 
> The container agent runs on each container instance within an Amazon ECS cluster. The agent sends information about the current running tasks and resource utilization of your containers to Amazon ECS. It starts and stops tasks whenever it receives a request from Amazon ECS.
>
> #### IAM Roles Needed in ECS Operations
>
> In Amazon ECS, you can create roles to grant permissions to Amazon ECS resource such as containers or services.
> - Task Execution Role (ECS Agent):
> *Think about what the ECS Agent needs to do on my behalf; pull a container image from Amazon ECR, ensure that I can log into CloudWatch Logs using the awslogs log driver, or when the task definition references sensitive data using Secrets Manager secrets or AWS Systems Manager Parameter Store parameters and make all of those AWS API calls.*

> - Task Role (Your Code/ Container):
> This role allows your application code (on the container) to use other AWS services, do you need your application to talk to an S3 Bucket, a DynamoDB table, or any API calls that your code itself needs to make to any other AWS services

> - Container Instance IAM Role
> Grants EC2 instance permission to communicate with the ECS Service

> - Amazon ECS EventBridge role
> You use the EventBridge rules and targets to schedule your tasks.

> - Amazon ECS infrastructure role
> You want to attach Amazon EBS volumes to your Fargate or EC2 launch type Amazon ECS tasks. The infrastructure role allows Amazon ECS to manage Amazon EBS volumes for your tasks. Or You want to use Transport Layer Security (TLS) to encrypt traffic between your Amazon ECS Service Connect services.

### ECS Task Definitions
- Task Definitions are metadata in JSON format to tell ECS how to Run a Docker Container.
### ECS Task
- A task is the instantiation of a task definition within a cluster. You can run a standalone task, or you can run a task as part of a service.

### ECS Service
- You can use an Amazon ECS service to run and maintain your desired number of tasks simultaneously in an Amazon ECS cluster. How it works is that, if any of your tasks fail or stop for any reason, the Amazon ECS service scheduler launches another instance based on your task definition. It does this to replace it and thereby maintain your desired number of tasks in the service.

### Architect your solution for Amazon ECS
> Before you use Amazon ECS, you need to make decisions about capacity, networking, account settings, and logging so that you can correctly configure your Amazon ECS resources.

#### Capacity
> The infrastructure capacity which can be a combination of the following:
- Amazon EC2 instances, Serverless (AWS Fargate (Fargate)), On-premises virtual machines (VM)

#### Networking
- The network (VPC and subnet) where your tasks and services run.

#### Feature access
> You can use your Amazon ECS account setting to access the following features: 
- Container Insights, awsvpc trunking, Tagging authorization, Fargate FIPS-140 compliance, Fargate task retirement time changes, Dual-stack VPC, Amazon Resource Name (ARN) format. 

#### Logging
> Logging and monitoring are important aspects of maintaining the reliability, availability, and performance of Amazon ECS workloads. The following options are available:
- Amazon CloudWatch logs - route logs to Amazon CloudWatch
- FireLens for Amazon ECS - route logs to an AWS service or AWS Partner Network destination for log storage and analysis.

- An optional namespace: used for service-to-service communication with Service Connect.
- A monitoring option: CloudWatch Container Insights comes at an additional cost and is a fully managed service. It automatically collects, aggregates, and summarizes Amazon ECS metrics and logs.

#### Amazon ECS capacity providers manage the scaling of infrastructure for tasks in your clusters. Each cluster can have one or more capacity providers and an optional capacity provider strategy. The capacity provider strategy determines how the tasks are spread across the cluster's capacity providers.

*When you run a standalone task or create a service, you either use the cluster's default capacity provider strategy or a capacity provider strategy that overrides the default one.*

### Clusters for the Fargate Launch Type
#### When you run your tasks on AWS Fargate, you do not need to create or manage the capacity. You just need to associate any of the following pre-defined capacity providers with the cluster:
- Fargate
- Fargate Spot (you can run interruption tolerant Amazon ECS tasks at a rate that's discounted compared to the Fargate price. Fargate Spot runs tasks on spare compute capacity.)

*Fargate is a serverless, pay-as-you-go compute engine that lets you focus on building applications without managing servers. When you choose Fargate, you don't need to manage an EC2 infrastructure.*

*A cluster can contain a mix of Fargate and Auto Scaling group capacity providers. However, a capacity provider strategy can only contain either Fargate or Auto Scaling group capacity providers, but not both.*

### Amazon ECS capacity providers for the EC2 launch type

#### You use Auto Scaling groups to manage the Amazon EC2 instances registered to their clusters. Auto Scaling helps ensure that you have the correct number of Amazon EC2 instances available to handle the application load.

*You can use the managed scaling feature to have Amazon ECS manage the scale-in and scale-out actions of the Auto Scaling group, or you can manage the scaling actions yourself. [For more information](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/cluster-auto-scaling.html)*

## Note
To create an empty Auto Scaling group, set the desired count to zero. After you created the capacity provider and associated it with a cluster, you can then scale it out.

#### Automatically scale your Amazon ECS service

*Automatic scaling is the ability to increase or decrease the desired count of tasks in your Amazon ECS service automatically. Amazon ECS leverages the Application Auto Scaling service to provide this functionality.*

Amazon ECS Service Auto Scaling supports the following types of automatic scaling:
- Target metric value: Increase or decrease the number of tasks that your service runs based on a target value for a specific metric.
- Predefined increments based on CloudWatch alarms: Increase or decrease the number of tasks that your service runs based on a set of scaling adjustments, known as step adjustments, that vary based on the size of the alarm breach.
- Schedule: Increase or decrease the number of tasks that your service runs based on the date and time.

#### Interconnect Amazon ECS services
*If you need an application to connect to other applications that run in Amazon ECS services, We recommend ECS Service Connect, which provides Amazon ECS configuration for service discovery, connectivity, and traffic monitoring. With Service Connect, your applications can use short names and standard ports to connect to Amazon ECS services in the same cluster, other clusters, including across VPCs in the same AWS Region.*

#### Feature access
> You can use your Amazon ECS account setting to access the following features:
> - Container Insights: CloudWatch Container Insights collects, aggregates, and summarizes metrics and logs from your containerized applications and microservices. The metrics include utilization for resources such as CPU, memory, disk, and network.
>
> - awsvpc trunking: For certain EC2 instances types, you can have additional network interfaces (ENIs) available on newly launched container instances.
>
> - Tagging authorization: Users must have permissions for actions that create a resource, such as ecsCreateCluster. If tags are specified in the resource-creating action, AWS performs additional authorization on the ecs:TagResource action to verify if users or roles have permissions to create tags.
>
> - Fargate FIPS-140 compliance: Fargate supports the Federal Information Processing Standard (FIPS-140) which specifies the security requirements for cryptographic modules that protect sensitive information. It is the current United States and Canadian government standard, and is applicable to systems that are required to be compliant with Federal Information Security Management Act (FISMA) or Federal Risk and Authorization Management Program (FedRAMP).
>
> - Fargate task retirement time changes: You can configure the wait period before Fargate tasks are retired for patching.
>
> - Dual-stack VPC: Allow tasks to communicate over IPv4, IPv6, or both.
>
> - Amazon Resource Name (ARN) format: Certain features, such as tagging authorization, require a new Amazon Resource Name (ARN) format.

#### Connect Amazon ECS applications to the internet
*Most containerized applications have a least some components that need outbound access to the internet. For example, the backend for a mobile app requires outbound access to push notifications.*

Amazon Virtual Private Cloud has two main methods for facilitating communication between your VPC and the internet.
- Private subnet and NAT gateway
- IPV6 egress only gateway

#### Best practices for receiving inbound connections to Amazon ECS from the internet
If you run a public service, you must accept inbound traffic from the internet.

*Have a scalable input layer that sits between the internet and your application. For this approach, you can use any of the AWS services listed in this section as an input.*

- Application Load Balancer
- Network Load Balancer
- Amazon API Gateway HTTP API


#### Best practices for connecting Amazon ECS to AWS services from inside your VPC
Two things are required to communicate with AWS services, permission and network connectivity:
- NAT gateway: NAT gateway is the easiest way to ensure that your Amazon ECS tasks can access other AWS services.
    - Disadvantages to using this approach: You can't limit what destinations the NAT gateway can communicate with and NAT gateways charge for every GB of data that passes through.
- Egress only internet gateway: 
- AWS PrivateLink: AWS PrivateLink provides private connectivity between VPCs, AWS services, and your on-premises networks without exposing your traffic to the public internet.

#### Monitoring Amazon ECS
Monitoring is an important part of maintaining the reliability, availability, and performance of Amazon ECS and your AWS solutions.

*You should collect monitoring data from all of the parts of your AWS solution so that you can more easily debug a multi-point failure if one occurs.*

The metrics made available depend on the launch type of the tasks and services in your clusters. If you are using the Fargate launch type for your services, then CPU and memory utilization metrics are provided to assist in the monitoring of your services. For the Amazon EC2 launch type, you own and need to monitor the EC2 instances that make your underlying infrastructure. Additional CPU and memory reservation and utilization metrics are made available at the cluster, service, and task.

Establish a baseline for normal Amazon ECS performance in your environment, by measuring performance at various times and under different load conditions. As you monitor Amazon ECS, store historical monitoring data so that you can compare it with current performance data, identify normal performance patterns and performance anomalies, and devise methods to address issues.

To establish a baseline you should, at a minimum, monitor the following items:
- The CPU and memory reservation and utilization metrics for your Amazon ECS clusters
- The CPU and memory utilization metrics for your Amazon ECS services

Best practices for monitoring Amazon ECS

#### Create a monitoring plan that includes answers to the following question:
- What are your monitoring goals? What resources will you monitor? How often will you monitor these resources? What monitoring tools will you use? Who will perform the monitoring tasks? Who should be notified when something goes wrong?

#### Automate monitoring as much as possible.

#### Automated monitoring tools

> You can use the following automated monitoring tools to watch Amazon ECS and report when something is wrong:
>
> - Amazon CloudWatch alarms - Watch a single metric over a time period that you specify, and perform one or more actions based on the value of the metric relative to a given threshold over a number of time periods. The action is a notification sent to an Amazon Simple Notification Service (Amazon SNS) topic or Amazon EC2 Auto Scaling policy.

For services with tasks that use the Fargate launch type, you can use CloudWatch alarms to scale in and scale out the tasks in your service based on CloudWatch metrics, such as CPU and memory utilization.

For your container instances that were launched with the Amazon ECS-optimized Amazon Linux AMI, you can use CloudWatch Logs to view different logs from your container instances in one convenient location. You must install the CloudWatch agent on your container instances.

You must also add the ECS-CloudWatchLogs policy to the ecsInstanceRole role.
>
> - Amazon CloudWatch Logs – Monitor, store, and access the log files from the containers in your Amazon ECS tasks by specifying the awslogs log driver in your task definitions. 

You can also monitor, store, and access the operating system and Amazon ECS container agent log files from your Amazon ECS container instances. This method for accessing logs can be used for containers using the EC2 launch type.

>
> - Amazon CloudWatch Events – Match events and route them to one or more target functions or streams to make changes, capture state information, and take corrective action.
>
> - Container Insights – Collect, aggregate, and summarize metrics and logs from your containerized applications and microservices. CloudWatch creates aggregated metrics at the cluster, task, and service level as CloudWatch metrics. The metrics that Container Insights collects are available in CloudWatch automatic dashboards, and are also viewable in the Metrics section of the CloudWatch console.
> 
> - Runtime Monitoring – Detect threats for clusters and containers within your AWS environment. Runtime Monitoring uses a GuardDuty security agent that adds runtime visibility into individual Amazon ECS workloads, for example, file access, process execution, and network connections.

*Amazon ECS provides free metrics for clusters and services. For an additional cost, you can turn on Amazon ECS CloudWatch Container Insights for your cluster for per-task metrics, including CPU, memory, and EBS filesystem utilization.*

The infrastructure your Amazon ECS tasks are hosted on in your clusters determines which metrics are available. For tasks hosted on Fargate infrastructure, Amazon ECS provides CPU, memory, and EBS filesystem utilization metrics are provided to assist in the monitoring of your services. For tasks hosted on EC2 instances, Amazon ECS provides CPU, memory, and GPU reservation metrics and CPU and memory utilization metrics at the cluster and service level. You need to monitor the Amazon EC2 instances that make your underlying infrastructure separately.

#### Recommended alarms to use with Amazon ECS

> Amazon ECS CloudWatch metrics: CPUReservation, CPUUtilization, MemoryReservation, MemoryUtilization, EBSFilesystemUtilization, GPUReservation, ActiveConnectionCount, NewConnectionCount, ProcessedBytes, RequestCount, GrpcRequestCount, HTTPCode_Target_2XX_Count, HTTPCode_Target_3XX_Count, HTTPCode_Target_4XX_Count, HTTPCode_Target_5XX_Count, RequestCountPerTarget, TargetProcessedBytes, TargetResponseTime, ClientTLSNegotiationErrorCount, TargetTLSNegotiationErrorCount.

When monitoring Amazon ECS (Elastic Container Service), it's essential to focus on key metrics that give you insights into the health, performance, and efficiency of your ECS clusters, services, and tasks. Here are some of the most important AWS ECS metrics you should set up alerts for:

## Cluster-Level Metrics

#### CPUUtilization (Cluster):
- Description: The percentage of CPU units currently used by the cluster.
- Why Important: High CPU utilization can indicate that your tasks are consuming a lot of CPU, potentially leading to performance degradation if it reaches critical levels.
- Alert Threshold: > 80% for a sustained period (e.g., 5 minutes).

#### MemoryUtilization (Cluster)
- Description: The percentage of memory currently used by the cluster.
- Why Important: High memory utilization can cause tasks to be throttled or terminated due to lack of memory.
- Alert Threshold: > 80% for a sustained period.

## Service-Level Metrics

#### CPUUtilization (Service)
- Description: The percentage of CPU units currently used by the service.
- Why Important: Helps in identifying CPU resource exhaustion for specific services.
- Alert Threshold: > 80% for a sustained period.

#### MemoryUtilization (Service)
- Description: The percentage of memory currently used by the service.
- Why Important: Indicates memory resource exhaustion for specific services, potentially leading to crashes or performance issues.
- Alert Threshold: > 80% for a sustained period.

#### RunningTaskCount
- Description: The number of tasks that are currently running in the service.
- Why Important: A decrease in running task count might indicate task failures or insufficient resources to start new tasks.
- Alert Threshold: Sudden drop in running task count.

#### PendingTaskCount
- Description: The number of tasks that are waiting to be placed on a container instance.
- Why Important: High pending task count can indicate resource constraints or misconfigurations preventing task placement.
- Alert Threshold: > 0 for a sustained period (depending on expected behavior).

## Task-Level Metrics

#### CPUUtilization (Task)
- Description: The percentage of CPU units currently used by individual tasks.
- Why Important: High CPU utilization at the task level can indicate that a particular task is over-consuming CPU resources.
- Alert Threshold: > 80% for a sustained period.

#### MemoryUtilization (Task)
- Description: The percentage of memory currently used by individual tasks.
- Why Important: High memory utilization at the task level can indicate memory leaks or tasks consuming more memory than expected.
- Alert Threshold: > 80% for a sustained period.

## Container Insights Metrics (if enabled)

#### ContainerInsightsCpuUtilization
- Metric: ContainerInsightsCpuUtilization
- Why Important: Provides detailed CPU utilization insights at the container level.
- Alert Threshold: > 80% for a sustained period.

#### ContainerInsightsMemoryUtilization
- Metric: ContainerInsightsMemoryUtilization
- Why Important: Provides detailed memory utilization insights at the container level.
- Alert Threshold: > 80% for a sustained period.

## Network Metrics

#### NetworkRxBytes
- Metric: NetworkRxBytes
- Why Important: Unusually high or low values can indicate network traffic issues.
- Alert Threshold: Sudden deviations from normal traffic patterns.

#### NetworkTxBytes
- Metric: NetworkTxBytes
- Why Important: Similar to NetworkRxBytes, monitors outgoing network traffic.
- Alert Threshold: Sudden deviations from normal traffic patterns.


## Summary of Must-Alert Metrics
Here are the essential metrics that you should set up alerts for:

#### Cluster-Level Metrics
- ClusterCPUUtilization > 80% for a sustained period.
- ClusterMemoryUtilization > 80% for a sustained period.

#### Service-Level Metrics
- ServiceCPUUtilization > 80% for a sustained period.
- ServiceMemoryUtilization > 80% for a sustained period.
- RunningTaskCount sudden drop below expected count.
- PendingTaskCount > 0 for a sustained period.

#### Task-Level Metrics
- TaskCPUUtilization > 80% for a sustained period.
- TaskMemoryUtilization > 80% for a sustained period.

#### Container Insights Metrics (if enabled)
- ContainerInsightsCpuUtilization > 80% for a sustained period.
- ContainerInsightsMemoryUtilization > 80% for a sustained period.

#### Network Metrics
- NetworkRxBytes sudden deviations from normal traffic patterns.
- NetworkTxBytes sudden deviations from normal traffic patterns.

By setting up alerts for these key metrics, you can proactively manage and maintain the health and performance of your ECS environment, ensuring that any issues are promptly identified and addressed.


## Logs
*When managing and monitoring an Amazon ECS (Elastic Container Service) environment, it's crucial to pay attention to various logs to ensure the health, performance, and security of your applications. Here are the key types of logs you should monitor:*

#### Application Logs
- Description: Logs generated by your application running inside the containers.
- Importance: These logs provide insights into the behavior and performance of your application, helping you diagnose errors, performance issues, and application-specific problems.
- Common Tools: Amazon CloudWatch Logs, Fluentd, Logstash.
- Key Aspects:
    - Error messages and stack traces, Performance bottlenecks and latency issues, Application-specific metrics and events.


#### Container Logs
- Description: Logs generated by the Docker containers themselves.
- Importance: These logs can help diagnose issues related to container startup, operation, and shutdown.
- Key Aspects:
    - Container startup and shutdown messages, Resource usage and limits reached, Container-specific errors and warnings.

#### ECS Agent Logs
- Description: Logs generated by the ECS agent running on each container instance.
- Importance: These logs provide information about the ECS agent’s interaction with the ECS control plane and can help diagnose issues related to task scheduling and state transitions.
- Key Aspects:
    - Task state transitions (starting, stopping), Communication with the ECS control plane, Errors and warnings from the ECS agent.


#### Load Balancer Logs
- Description: Logs generated by the load balancers (e.g., ELB, ALB, NLB) distributing traffic to your ECS tasks.
- Importance: These logs provide visibility into traffic patterns, client access, and potential load balancing issues.
- Key Aspects:
    - Request and response details, Latency and error rates, Traffic patterns and anomalies.


#### Prioritizing Logs
Critical Logs to Monitor Regularly:

- Application Logs: For diagnosing application-specific issues.
- Container Logs: For container-specific operations and errors.
- ECS Agent Logs: For task state and scheduling issues.
- Load Balancer Logs: For monitoring traffic distribution and access patterns.

#### Setting Up Log Monitoring
- Alerts and Alarms: Set up CloudWatch Alarms or similar tools to trigger alerts based on log patterns that indicate issues (e.g., high error rates, resource limits reached).
- Dashboards: Create CloudWatch Dashboards to visualize key log metrics and trends.
- Log Retention and Archiving: Ensure logs are retained for an appropriate period for compliance and forensic analysis, using Amazon S3 or other storage solutions.

#### By closely monitoring these logs, you can maintain the health, performance, and security of your ECS environment and quickly respond to any issues that arise.


## Summary of Essential Elements for Dashboard and Alerts

#### Metrics (based on queries)
AppErrorCount: Number of application errors.
ContainerWarnErrorCount: Number of warnings/errors in container logs.
AgentErrorCount: Number of errors in ECS agent logs.

#### Logs Queries as Metrics
Application Logs: Query for ERROR logs and count them.
Container Logs: Query for WARN and ERROR logs and count them.
ECS Agent Logs: Query for error and failure logs and count them.

#### Alerts
AppErrorCountAlarm: Alert if application errors exceed a certain threshold.
ContainerWarnErrorCountAlarm: Alert if container warnings/errors exceed a certain threshold.
AgentErrorCountAlarm: Alert if ECS agent errors exceed a certain threshold.

## Scheduled tasks

A scheduled task is suitable when you have tasks to run at set intervals in your cluster, you can use EventBridge Scheduler to create a schedule.