
provider "aws" {
  region = local.region
}

locals {
  region = "us-east-1"
}

################################################################################
# Cluster
################################################################################

module "ecs" {
  source = "../../modules/cluster"
  cluster_name = "test"

    cluster_configuration = {
    execute_command_configuration = {
      logging = "OVERRIDE"
      log_configuration = {
        cloud_watch_log_group_name = "/aws/ecs/aws-ec2"
      }
    }
  }

    fargate_capacity_providers = {
    FARGATE = {
      default_capacity_provider_strategy = {
        weight = 50
      }
    }
    FARGATE_SPOT = {
      default_capacity_provider_strategy = {
        weight = 50
      }
    }
  }

  tags = {
    Environment = "Development"
    Project     = "EcsFargate"
  }
}

